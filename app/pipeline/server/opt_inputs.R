#' Event when use testButton button
observeEvent(input$testButton, {

})

#' Event when use testButton2 button
observeEvent(input$testButton2, {

})

#' Event when use inputs_testButton button
observeEvent(input$inputs_testButton, {

})

#' Event when use inputs_testButton button
observeEvent(input$inputs_testButton, {

})

#' Event when use runP button
observeEvent(input$runP, {

# Params 
	path_param <- "/home/jimmy/jimmy/projets/sag/app/pipeline/params/params_inputs.yml"

	res <- ""

	# Panel : panel1

	# Tool : A

	if(input$selectpanel1 == "A") {
		if(!is.na(as.numeric(input$param1))) {
			res <- paste(res, "param1:", input$param1, "\n", sep = " ")
		} else {
			res <- paste(res, "param1:", paste0('"', input$param1, '"'), "\n", sep = " ")
		}	

		res <- paste(res, "param2:", input$param2, "\n", sep = " ")	

		res <- paste(res, "param3_min:", input$param3[1], "\n", sep = " ")
		res <- paste(res, "param3_max:", input$param3[2], "\n", sep = " ")
		res <- paste(res, "param3_step:", "0.1", "\n", sep = " ")
	

		if(input$param8) {
			res <- paste(res, "param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "param8:", "false", "\n", sep = " ")
		}	

		if(input$param9) {
			res <- paste(res, "param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "param9:", "false", "\n", sep = " ")
		}	

		res <- paste0(res, " mychoose2" + "_left: [")
		if(length(input$mychoose2$left) > 0) {
		for(x in 1:length(input$mychoose2$left)) {
			res <- paste0(res, '"', input$mychoose2$left[[x]], '"')
			if(x < length(input$mychoose2$left)) {
				res <- paste0(res, ", ")
			}
		}
		}		
		res <- paste0(res, "]", "\n")

		res <- paste0(res, " mychoose2" + "_right: [")
		if(length(input$mychoose2$right) > 0) {
		for(x in 1:length(input$mychoose2$right)) {
			res <- paste0(res, '"', input$mychoose2$right[[x]], '"')
			if(x < length(input$mychoose2$right)) {
				res <- paste0(res, ", ")
			}
		}
		}		
		res <- paste0(res, "]", "\n")

	}


	# Tool : B

	if(input$selectpanel1 == "B") {
		if(!is.na(as.numeric(input$param4))) {
			res <- paste(res, "param4:", input$param4, "\n", sep = " ")
		} else {
			res <- paste(res, "param4:", paste0('"', input$param4, '"'), "\n", sep = " ")
		}	

		if(!is.na(as.numeric(input$param5))) {
			res <- paste(res, "param5:", input$param5, "\n", sep = " ")
		} else {
			res <- paste(res, "param5:", paste0('"', input$param5, '"'), "\n", sep = " ")
		}	

	}


	# Panel : panel2

	# Tool : C

	if(input$selectpanel2 == "C") {
		if(!is.na(as.numeric(input$param10))) {
			res <- paste(res, "param10:", input$param10, "\n", sep = " ")
		} else {
			res <- paste(res, "param10:", paste0('"', input$param10, '"'), "\n", sep = " ")
		}	

			res <- paste(res, "param7:", paste0('"', input$param7$datapath, '"'), "\n", sep = " ")	

	}


	# Panel : panel3

	# Tool : biotoolB

	if(input$selectpanel3 == "biotoolB") {
		if(!is.na(as.numeric(input$inputs_param1))) {
			res <- paste(res, "inputs_param1:", input$inputs_param1, "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param1:", paste0('"', input$inputs_param1, '"'), "\n", sep = " ")
		}	

		res <- paste(res, "inputs_param2:", input$inputs_param2, "\n", sep = " ")	

		res <- paste(res, "inputs_param3_min:", input$inputs_param3[1], "\n", sep = " ")
		res <- paste(res, "inputs_param3_max:", input$inputs_param3[2], "\n", sep = " ")
		res <- paste(res, "inputs_param3_step:", "0.1", "\n", sep = " ")
	

		if(input$inputs_param8) {
			res <- paste(res, "inputs_param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param8:", "false", "\n", sep = " ")
		}	

		if(input$inputs_param9) {
			res <- paste(res, "inputs_param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param9:", "false", "\n", sep = " ")
		}	

	}


	# Tool : biotoolA

	if(input$selectpanel3 == "biotoolA") {
		if(!is.na(as.numeric(input$inputs_param1))) {
			res <- paste(res, "inputs_param1:", input$inputs_param1, "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param1:", paste0('"', input$inputs_param1, '"'), "\n", sep = " ")
		}	

		if(input$inputs_param8) {
			res <- paste(res, "inputs_param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param8:", "false", "\n", sep = " ")
		}	

		if(input$inputs_param9) {
			res <- paste(res, "inputs_param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param9:", "false", "\n", sep = " ")
		}	

	}


	# Panel : panel4

	# Panel : panel5
	write(res, file=path_param)

	system(paste(" python", "", "/home/jimmy/jimmy/projets/sag/app/mon_script.py" ,  "-i3", input$param6 ,  "-t", input$param1 ,  "", input$param4 ,  "-path1", input$param7$datapath ,  "-params_file", "/home/jimmy/jimmy/projets/sag/app/pipeline/params/params_inputs.yml", sep = " " ))

	if(file.exists("/home/jimmy/jimmy/projets/sag/app/result.Rmd")) {
		output$report_inputs <- renderUI({
			 includeMarkdown(knit("/home/jimmy/jimmy/projets/sag/app/result.Rmd", quiet = TRUE))
		})
	}

})


